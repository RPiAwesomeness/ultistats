import { ReactElement, useMemo, useCallback } from "react";
import { SectionListData, Alert, SectionList, StyleSheet, Pressable } from "react-native";
import { Text, View } from './Themed';
import { Ionicons } from "@expo/vector-icons";

export interface Player {
  firstName: string;
  lastName: string;
  jerseyNum: number;
}

interface PlayerListProps {
  players: Player[] | undefined;
  onPlayerRemove?: (player: Player) => Promise<boolean>;
}

function PlayerList({ players, onPlayerRemove }: PlayerListProps): ReactElement {
  // TODO: Refactor this disaster to be more efficient but it accomplished its goal so we're leaving it for now
  const sections = useMemo<SectionListData<Player>[]>(
    () => {
      if (!players) return [];
      
      const groups = new Set([...players].map(({ lastName }) => lastName.at(0)?.toUpperCase() || 'Unknown'));
      const sections = Object.fromEntries([...groups].map<[string, Set<Player>]>(g => [g, new Set()]));
      players.forEach((player) => {
        const { lastName } = player;
        const group = lastName.at(0)?.toUpperCase() || 'Unknown';
        sections[group].add(player);
      });
      return Object
        .entries(sections)
        .map<SectionListData<Player>>(([title, data]) => ({ title, key: title, data: [...data] }))
        .sort(({ key: lhsKey = 'Unknown' }, { key: rhsKey = 'Unknown' }) => lhsKey.localeCompare(rhsKey));
    },
    [players],
  );
  const removePlayer = useCallback((player: Player) => {
    if (onPlayerRemove?.(player)) {
      Alert.alert('Removing player', `Removed ${player.firstName} ${player.lastName}!`)
    } else {
      Alert.alert('Removing player', `Failed to remove ${player.firstName} ${player.lastName}!`)
    }
  }, [onPlayerRemove]);

  return (
    <View style={styles.container}>
      <SectionList
        style={styles.sectionList}
        sections={sections}
        renderItem={({ item: player }) => {
          const { firstName, lastName, jerseyNum } = player;
          return (
            <View style={styles.item}>
              <Text>{firstName} {lastName} ({jerseyNum})</Text>
              <Pressable onPress={() => removePlayer(player)} android_ripple={{ color: 'white', borderless: true }}>
                <Ionicons size={28} name="remove-circle-outline" color="white" />
              </Pressable>
            </View>
          );
        }}
        renderSectionHeader={({ section }) => (
          <Text style={styles.sectionHeader}>{section.title}</Text>
        )}
        keyExtractor={({ firstName, lastName, jerseyNum }) => `player-${firstName}-${lastName}-${jerseyNum}`}
        ListEmptyComponent={() => <Text style={styles.sectionHeader}>No Players on the Team!</Text>}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  sectionList: {
    flex: 1,
    width: '100%'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
    color: 'black',
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    height: 64,
  },
});

export default PlayerList;