import { Alert, Pressable, SafeAreaView, StyleSheet } from 'react-native';

import { Text, View } from '../../components/Themed';
import PlayerList from '../../components/PlayerList';
import { useCallback, useContext } from 'react';
import AppContext from '../AppContext';
import { Ionicons } from '@expo/vector-icons';

export default function TabTwoScreen() {
  const { players, removePlayer } = useContext(AppContext);

  const newPlayer = useCallback(() => {
    Alert.alert('Adding a player');
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        <Text style={styles.title}>Manage your team members</Text>
        <Pressable style={styles.hugButton} onPress={newPlayer} android_ripple={{ color: 'white' }}>
          <Ionicons size={20} name="person-add" color="white" />
        </Pressable>
      </View>
      <PlayerList players={players} onPlayerRemove={removePlayer} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sectionList: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  hugButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 5,
  },
});
