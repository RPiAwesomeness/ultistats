import FontAwesome from '@expo/vector-icons/FontAwesome';
import { DarkTheme, DefaultTheme, ThemeProvider } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import { SplashScreen, Stack } from 'expo-router';
import { useCallback, useEffect, useState } from 'react';
import { Alert, useColorScheme } from 'react-native';
import AppContext from './AppContext';
import { Player } from '../components/PlayerList';
import AsyncStorage from '@react-native-async-storage/async-storage';

export {
  // Catch any errors thrown by the Layout component.
  ErrorBoundary,
} from 'expo-router';

export const unstable_settings = {
  // Ensure that reloading on `/modal` keeps a back button present.
  initialRouteName: 'tabs',
};

// Prevent the splash screen from auto-hiding before asset loading is complete.
SplashScreen.preventAutoHideAsync();

export default function RootLayout() {
  const [loaded, error] = useFonts({
    SpaceMono: require('../assets/fonts/SpaceMono-Regular.ttf'),
    ...FontAwesome.font,
  });

  // Expo Router uses Error Boundaries to catch errors in the navigation tree.
  useEffect(() => {
    if (error) throw error;
  }, [error]);

  useEffect(() => {
    if (loaded) {
      SplashScreen.hideAsync();
    }
  }, [loaded]);

  if (!loaded) {
    return null;
  }

  return <RootLayoutNav />;
}

const TEST_PLAYERS: Player[] = [
  {
    firstName: 'Sgt.',
    lastName: 'Fitch',
    jerseyNum: 23
  },
  {
    firstName: 'Nate',
    lastName: 'Olander',
    jerseyNum: 8,
  },
  {
    firstName: 'Riley',
    lastName: 'Ulmer',
    jerseyNum: 10,
  },
  {
    firstName: 'Big',
    lastName: 'Dan',
    jerseyNum: 14,
  },
  {
    firstName: 'Riley',
    lastName: 'Collins',
    jerseyNum: 0
  },
  {
    firstName: 'Taylor',
    lastName: 'Chatfield',
    jerseyNum: 16
  },
  {
    firstName: 'Chetan',
    lastName: 'Gomatam',
    jerseyNum: 31
  }
];

function RootLayoutNav() {
  const colorScheme = useColorScheme();
  const [players, setPlayers] = useState<Player[]>([]);

  // Load players once on startup
  useEffect(() => {
    try {
      AsyncStorage.getItem('players', (err, result) => {
        if (err || !result) return;

        const playerJson = JSON.parse(result);
        setPlayers(playerJson);
      });
    } catch (e) {
      null;
    }
  }, []);

  const addPlayer = useCallback(async (player: Player) => {
    const hasPlayer = players.includes(player);

    // Did not add it because it already exists
    if (hasPlayer) return false;

    // Write new player list to storage
    const newPlayers = [...players, player];
    try {
      const playerDumps = JSON.stringify(newPlayers);
      await AsyncStorage.setItem('players', playerDumps);
    } catch (e) {
      Alert.alert('Storage Error', `Failed to add new player: ${e}`);
      return false;
    }
    setPlayers(newPlayers);
    return true;
  }, [players]);
  const removePlayer = useCallback(async (player: Player) => {
    const hasPlayer = players.includes(player);

    // Did not remove it because it doesn't exist
    if (!hasPlayer) return false;
   
    // Write to storage, removing player
    const newPlayers = players.filter(p => p !== player);
    try {
      const playerDumps = JSON.stringify(newPlayers);
      await AsyncStorage.setItem('players', playerDumps);
    } catch (e) {
      Alert.alert('Storage Error', `Failed to remove player: ${e}`);
      return false;
    }
    setPlayers(newPlayers);
    return true;
  }, [players]);

  return (
    <ThemeProvider value={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <AppContext.Provider value={{
        players,
        addPlayer,
        removePlayer,
      }}>
        <Stack>
          <Stack.Screen name="tabs" options={{ headerShown: false }} />
        </Stack>
      </AppContext.Provider>
    </ThemeProvider>
  );
}
