import { createContext } from "react";
import { Player } from "../components/PlayerList";

export interface AppContext {
    players?: Player[];
    addPlayer?: (player: Player) => Promise<boolean>;
    removePlayer?: (player: Player) => Promise<boolean>;
}

export default createContext<AppContext>({
    players: undefined,
    addPlayer: undefined,
    removePlayer: undefined,
});
